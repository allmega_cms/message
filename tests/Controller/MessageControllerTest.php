<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega 
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\Tests\Controller;

use Allmega\MessageBundle\Controller\MessageController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\MessageBundle\Entity\Message;
use Allmega\BlogBundle\Utils\Helper;
use Allmega\MessageBundle\Data;

class MessageControllerTest extends AllmegaWebTest
{
    public function testDashboardWidget(): void
    {
        $this->runTests($this->dashboard, Data::USER_ROLE, false, false);
    }

    public function testMenuIcon(): void
    {
        $this->runTests($this->icon, Data::USER_ROLE, false, false);
    }

    public function testIndex(): void
    {
        $this->runTests($this->index, Data::USER_ROLE);
    }

    public function testAdd(): void
    {
        $this->runTests($this->add, Data::USER_ROLE);
    }

    public function testAnswer(): void
    {
        $this->create();
        $optParams = [
            'answer' => Helper::generateRandomString(),
            'id' => $this->params->getEntity()->getId(),
        ];

        $this->runTests(
            optParams: $optParams,
            role: Data::USER_ROLE,
            routeName: 'answer',
            method: 'POST',
            xmlHttp: true,
            create: true);

        $response = $this->getJsonResponse();
        $this->assertArrayHasKey('success', $response);
    }

    public function testDelete(): void
    {
        $this->assertEquals(true,true);
    }

    protected function create(): void
    {
        $user = $this->findUserByRole(Data::USER_ROLE);
        $entity = Message::build(sender: $user);
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return MessageController::ROUTE_NAME.$name;
    }
}