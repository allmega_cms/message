<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega 
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\EventSubscriber;

use Allmega\MessageBundle\{Data, Events};
use Allmega\BlogBundle\Utils\Params\NotificationParams;
use Symfony\Component\EventDispatcher\{EventSubscriberInterface, GenericEvent};
use Symfony\Component\Messenger\Exception\ExceptionInterface;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class NotificationSubscriber implements EventSubscriberInterface
{
    public function __construct(private MessageBusInterface $bus) {}

    public static function getSubscribedEvents(): array
    {
        return [
            Events::MESSAGE_RECEIVED => 'onMessageReceived',
        ];
    }

    /**
     * @throws ExceptionInterface
     */
    public function onMessageReceived(GenericEvent $event): void
    {
        $message = $event->getSubject();
        $search  = ['%username%'];
        $replace = [$message->getAuthor()->getFullname()];

        $params = new NotificationParams(Data::NOTIFICATION_MESSAGE_RECEIVED, [], $search, $replace);
        foreach ($message->getChat()->getMembers() as $receiver) $params->addReceiver($receiver->getEmail());
        $this->bus->dispatch($params);
    }
}