<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega 
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\EventSubscriber;

use Allmega\MessageBundle\Events;
use Allmega\BlogBundle\Model\FlashesTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FlashSubscriber implements EventSubscriberInterface
{
    use FlashesTrait;

    public static function getSubscribedEvents(): array
    {
        return [
            Events::MESSAGE_UPDATED => 'addSuccessFlash',
            Events::MESSAGE_DELETED => 'addSuccessFlash',
            Events::CHAT_CREATED => 'addSuccessFlash',
            Events::CHAT_UPDATED => 'addSuccessFlash',
            Events::CHAT_DELETED => 'addSuccessFlash',
        ];
    }
}