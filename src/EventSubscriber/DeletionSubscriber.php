<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega 
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\EventSubscriber;

use Allmega\AuthBundle\Entity\User;
use Allmega\AuthBundle\Events as AuthEvents;
use Allmega\MessageBundle\Entity\{Chat, Message};
use Symfony\Component\EventDispatcher\{EventSubscriberInterface, GenericEvent};
use Doctrine\ORM\EntityManagerInterface;

class DeletionSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly EntityManagerInterface $em) {}

    public static function getSubscribedEvents(): array
    {
        return [
            AuthEvents::USER_DELETE => 'handleUserRelations',
        ];
    }

    public function handleUserRelations(GenericEvent $event, string $eventName): void
    {
        $userRepo = $this->em->getRepository(User::class);
        $user = $event->getSubject();

        $userRepo
            ->updateField(Message::class, 'author', $user)
            ->updateField(Chat::class, 'author', $user);
    }
}