<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega 
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle;

final class Events
{
    public const CHAT_CREATED = 'chat.created';
    public const CHAT_UPDATED = 'chat.updated';
    public const CHAT_DELETED = 'chat.deleted';

    public const MESSAGE_RECEIVED = 'message.received';
    public const MESSAGE_UPDATED = 'message.updated';
    public const MESSAGE_DELETED = 'message.deleted';
}