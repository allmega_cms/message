<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\Controller;

use Allmega\MessageBundle\Entity\Chat;
use Allmega\MessageBundle\{Data, Events};
use Allmega\BlogBundle\Utils\Search\SearchItem;
use Allmega\MessageBundle\Repository\ChatRepository;
use Allmega\MessageBundle\Manager\ChatControllerTrait;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route(name: 'allmega_chat_')]
class ChatController extends BaseController
{
    use ChatControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaMessage/chat/';
    public const ROUTE_NAME = 'allmega_chat_';

    public function __construct(
        private readonly ChatRepository $chatRepo,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/search', name: 'search', methods: 'GET')]
    #[IsGranted('message-chat-search')]
    public function search(SearchItem $item): Response
    {
        $item
            ->setTemplatePath(self::ROUTE_TEMPLATE_PATH)
            ->setRoute(self::ROUTE_NAME . 'show')
            ->setProps(['title'])
            ->getRepo()
            ->setSearch($this->chatRepo)
            ->addOptions(['user' => $this->getUser()]);

        $item->getTrans()->setLabel('chat.search.title')->setDomain(Data::DOMAIN);
        return $this->json(['data' => $item->getSearchResult()]);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('message-chat-list')]
    public function index(): Response
    {
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'chats' => $this->chatRepo->findByMember($this->getUser()),
        ]);
    }

    #[Route('/show/{id}', name: 'show', methods: ['GET', 'POST'])]
    #[IsGranted('message-chat-show', subject: 'chat')]
    public function show(Chat $chat): Response
    {
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'chat' => $chat
        ]);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('message-chat-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: 'GET|POST')]
    #[IsGranted('message-chat-edit', subject: 'chat')]
    public function edit(Chat $chat): Response
    {
        return $this->save($chat);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('message-chat-delete', subject: 'chat')]
    public function delete(Chat $chat): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $chat,
            domain: Data::DOMAIN,
            eventName: Events::CHAT_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/dashboard', name: 'dashboard', methods: 'GET')]
    public function getDashboardWidget(): Response
    {
        if (!$this->isGranted('message-chat-dashboard')) return new Response();

        $user = $this->getUser();
        $chats = $this->chatRepo->findMemberChats($user, false, 3);
        $latestNum = $this->chatRepo->findMemberChats($user, true);
        $num = $this->chatRepo->findMemberChats($user, true);

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'dashboard.html.twig', [
            'chats' => $chats, 'latestNum' => $latestNum, 'num' => $num,
            'params' => $this->getTemplateParams($this, Data::DOMAIN)
        ]);
    }

    #[Route('/icon', name: 'icon', methods: 'GET')]
    public function getMenuIcon(): Response
    {
        if (!$this->isGranted('message-chat-icon')) return new Response();

        $unreaded = $this->chatRepo->findMemberChats($this->getUser(), true);
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'inc/_icon.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'unreaded' => $unreaded
        ]);
    }
}