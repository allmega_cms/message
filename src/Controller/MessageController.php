<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega 
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\Controller;

use Allmega\MessageBundle\{Data, Events};
use Allmega\MessageBundle\Entity\{Chat, Message};
use Allmega\MessageBundle\Repository\MessageRepository;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\MessageBundle\Manager\MessageControllerTrait;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route(name: 'allmega_message_')]
class MessageController extends BaseController
{
    use MessageControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaMessage/message/';
    public const ROUTE_NAME = 'allmega_message_';

    public function __construct(
        private readonly MessageRepository $messageRepo,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/add/{id}/{parentId}', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('message-add', subject: 'chat')]
    public function add(Chat $chat, ?string $parentId = null): Response
    {
        return $this->save(null, $chat);
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('message-edit', subject: 'message')]
    public function edit(Message $message): Response
    {
        return $this->save($message);
    }

    #[Route('/delete/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('message-delete', subject: 'message')]
    public function delete(Message $message): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $message,
            domain: Data::DOMAIN,
            eventName: Events::MESSAGE_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }
}