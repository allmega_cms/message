<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\Form;

use Allmega\MessageBundle\Data;
use Allmega\MessageBundle\Entity\Chat;
use Allmega\AuthBundle\Model\UsersTrait;
use Allmega\AuthBundle\Repository\UserRepository;
use Allmega\AuthBundle\Utils\Params\UsersTraitParams;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\SecurityBundle\Security;

class ChatType extends AbstractType
{
    use UsersTrait;

    public function __construct(
        private readonly UsersTraitParams $usersParams,
        private readonly UserRepository $userRepo,
        private readonly Security $security) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        extract($this->createOptions());
        $builder
            ->add('members', EntityType::class, $members)
            ->add('title', null, [
                'attr' => ['autofocus' => true, 'placeholder' => 'chat.label.title'],
                'label' => 'chat.label.title'
            ])
            ->add('description', null, [
                'attr' => ['placeholder' => 'chat.label.description'],
                'label' => 'chat.label.description'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Chat::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }

    private function createOptions(): array
    {
        $uid = $this->security->getUser()->getId();
        $this->usersParams
            ->setLabel('chat.label.members')
            ->setHelp('chat.help.members')
            ->setRoles([Data::USER_ROLE])
            ->setMultiple(true);

        $this->setUsersQueryBuilder();
        $this->usersParams
            ->getQueryBuilder()
            ->andWhere('u.id != :id')
            ->setParameter('id', $uid);

        return ['members' => $this->getUsersOptions()];
    }
}