<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega 
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\Form;

use Allmega\MessageBundle\Data;
use Allmega\MessageBundle\Entity\Message;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
	{
		$builder
			->add('text', TextareaType::class, [
				'attr' => ['rows' => 5, 'placeholder' => 'label.message'],
				'label' => 'label.message',
			]);
	}

	public function configureOptions(OptionsResolver $resolver): void
	{
		$resolver->setDefaults([
			'data_class' => Message::class,
            'translation_domain' => Data::DOMAIN
        ]);
	}
}