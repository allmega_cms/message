<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\Repository;

use Allmega\AuthBundle\Entity\User;
use Allmega\MessageBundle\Entity\Chat;
use Allmega\BlogBundle\Model\SearchableInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTime;

class ChatRepository extends ServiceEntityRepository implements SearchableInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Chat::class);
    }

    public function findByMember(User $user, bool $count = false, int $limit = 0): int|array
    {
        $query = $this->createQueryBuilder('c');

        if ($count) $query->select('COUNT(c.id)');
        elseif ($limit > 0) $query->setMaxResults($limit);

        $query = $query
            ->join('c.members', 'u')
            ->where('u.id = :id')
            ->setParameter('id', $user->getId())
            ->getQuery();

        return $count ? $query->getSingleScalarResult() : $query->getResult();
    }

    public function findMemberChats(User $user, bool $count = false, int $limit = 0): int|array
    {
        $query = $this->createQueryBuilder('c')
            ->join('c.messages', 'm')
            ->join('c.members', 'u')
            ->where('u.id = :id')
            ->setParameter('id', $user->getId())
            ->andWhere('m.readed = 0');

        if ($limit > 0) $query->setMaxResults($limit);
        if ($count) {
            $query
                ->select('COUNT(m.id)')
                ->andWhere('m.created BETWEEN :last AND :now')
                ->setParameter('last', $user->getLastSeen()->format('Y-m-d H:i:S'))
                ->setParameter('now', (new DateTime())->format('Y-m-d H:i:s'));
        }

        $query = $query->getQuery();
        return $count ? $query->getSingleScalarResult() : $query->getResult();
    }

    public function findBySearchQuery(array $terms, array $options, int $limit): array
    {
        if (0 === count($terms)) return [];

        extract($options);
        // [$user]

        $query = $this->createQueryBuilder('c')
            ->join('c.members', 'u')
            ->where('u.id = :id')
            ->setParameter('id', $user->getId());

        foreach ($terms as $key => $term) {
            $query
                ->orWhere('c.title LIKE :t_'.$key)->setParameter('t_'.$key, '%'.$term.'%')
                ->orWhere('c.description LIKE :d_'.$key)->setParameter('d_'.$key, '%'.$term.'%');
        }

        return $query
            ->orderBy('c.title', 'ASC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}