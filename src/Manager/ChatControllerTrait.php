<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\Manager;

use Allmega\MessageBundle\Entity\Chat;
use Allmega\MessageBundle\Form\ChatType;
use Allmega\MessageBundle\{Data, Events};
use Allmega\MessageBundle\Controller\ChatController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait ChatControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
            case $this->delete:
                break;
            default:
        }
        return $this;
    }

    protected function postExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
            case $this->delete:
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
                $this->params
                    ->addRouteParams(['id' => $this->params->getEntity()->getId()])
                    ->setRouteName($this->params->getRouteShort(), $this->show);
                break;
            default:
        }
        return $this;
    }

    private function save(Chat $chat = null, array $arguments = []): Response
    {
        $eventName = $chat ? Events::CHAT_UPDATED : Events::CHAT_CREATED;
        $chat = $chat ?? new Chat();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: ['item' => $chat],
            entity: $chat,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: ChatType::class,
            routeName: ChatController::ROUTE_NAME,
            templatesPath: ChatController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}