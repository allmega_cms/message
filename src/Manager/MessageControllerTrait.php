<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega 
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\Manager;

use Allmega\MessageBundle\Entity\Message;
use Allmega\BlogBundle\Utils\Helper;

trait MessageControllerTrait
{
    private function setMessagesAsReaded(array $messages): static
    {
        if (count($messages)) {
            $em = $this->getEntityManager();
            $uid = $this->getUser()->getId();

            foreach ($messages as $message) {
                foreach ($message->getReceivers() as $receiver) {
                    if ($uid == $receiver->getId()) {
                        $message->setReadedAt(new \DateTime());
                        $em->persist($message);
                    }
                }
            }
            $em->flush();
        }
        return $this;
    }

    private function saveAnswer(Message $message, string $text): Message
    {
        $em = $this->getEntityManager();
        $answer = (new Message())
            ->addReceiver($message->getSender())
            ->setSender($this->getUser())
            ->setParent($message)
            ->setText($text);

        $em->persist($answer);
        $em->flush();
        return $answer;
    }

    private function deleteMessages(string $id): void
    {
        // Create the same structure as presented to the user so that
        // all messages belonging to an entire chat can be deleted
        $results = $this->messageRepo->findAllMessages($this->getUser());

        // Sort messages
        $results = $this->sortMessages($results);

        // If the initial ID is the same as the one to be deleted,
        // temporary storage in $ tmp for further processing
        $tmp = [];
        foreach ($results as $unit) {
            if ($id == $unit['id']) {
                $tmp = $unit;
                break;
            }
        }

        // Find out the IDs of the messages to be deleted
        $ids = explode(',', $this->getIds($tmp));
        $this->messageRepo->deleteAll($ids);
    }

    private function sortMessages(array $messages): array
    {
        $tree = Helper::sort($messages);
        $sorted = [];

        foreach ($tree as $unit) {
            $name = $this->getUser()->getId() != $unit['row']->getSender()->getId() ?
                $unit['row']->getSender()->getUsername() :
                implode(', ', $unit['row']->getReceiversNames());

            $name .= '|' . substr(md5(uniqid(rand())), 0, 15);
            $sorted[$name] = $unit;
        }
        return $sorted;
    }

    /** Write the ID of each individual message in a chat in a comma separated string */
    private function getIds(array $unit): string
    {
        $ids = $unit['id'];
        if (count($unit['childs'])) {
            foreach ($unit['childs'] as $row) $ids .= ',' . $this->getIds($row);
        }
        return $ids;
    }
}