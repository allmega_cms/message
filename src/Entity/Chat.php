<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\Entity;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\MessageBundle\Repository\ChatRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\{ArrayCollection, Collection};
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;
use DateTimeInterface;
use DateTime;

#[ORM\Entity(repositoryClass: ChatRepository::class)]
#[ORM\Table(name: '`allmega_message__chat`')]
class Chat
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'message.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $title = null;

    #[ORM\Column(length: 191, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?DateTimeInterface $created = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $author = null;

    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: '`allmega_message__chat_user`')]
    #[ORM\OrderBy(['lastname' => 'ASC'])]
    private Collection $members;

    #[ORM\OneToMany(targetEntity: Message::class, mappedBy: 'chat', orphanRemoval: true)]
    #[ORM\OrderBy(['created' => 'ASC'])]
    private Collection $messages;

    /**
     * Create a new Chat entity with predetermined data,
     * if no data is provided, it will be generated:
     *  - $title, $description as dummy text
     * - $author will be created
     */
    public static function build(
        string $description = null,
        string $title = null,
        User $author = null): static
    {
        $description = $description ?? Helper::getLoremIpsum();
        $title = $title ?? Helper::generateRandomString();
        $author = $author ?? User::build();

        return (new static())
            ->setDescription($description)
            ->setAuthor($author)
            ->setTitle($title);
    }

    public function __construct()
    {
        $this->messages = new ArrayCollection();
        $this->members = new ArrayCollection();
        $this->created = new DateTime();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    public function getCreated(): DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(?DateTimeInterface $created = null): static
    {
        $this->created = $created ?? new DateTime();
        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): static
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    public function addMember(User $member): static
    {
        if (!$this->members->contains($member)) {
            $this->members->add($member);
        }
        return $this;
    }

    public function removeMember(User $member): static
    {
        $this->members->removeElement($member);
        return $this;
    }

    /**
     * @return Collection<int, Message>
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): static
    {
        if (!$this->messages->contains($message)) {
            $this->messages->add($message);
            $message->setChat($this);
        }
        return $this;
    }

    public function removeMessage(Message $message): static
    {
        if ($this->messages->removeElement($message)) {
            if ($message->getChat() === $this) {
                $message->setChat(null);
            }
        }
        return $this;
    }
}