<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega 
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\Entity;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\MessageBundle\Repository\MessageRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;
use DateTimeInterface;
use DateTime;

#[ORM\Entity(repositoryClass: MessageRepository::class)]
#[ORM\Table(name: '`allmega_message__message`')]
class Message
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\ManyToOne(targetEntity: Message::class)]
    protected ?Message $parent = null;

    #[ORM\ManyToOne(inversedBy: 'messages')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Chat $chat = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $author = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message: 'message.blank')]
    #[Assert\Length(max: 2000, maxMessage: 'errors.max_value')]
    private ?string $text = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?DateTimeInterface $created = null;

    #[ORM\Column]
    private bool $readed = false;

    /**
     * Create a new Message entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $author will be 
     * - $text as dummy text
     */
    public static function build(
        string $text = null,
        User $author = null,
        Chat $chat = null): static
    {
        $text = $text ?? Helper::getLoremIpsum();
        $author = $author ?? User::build();
        $chat = $chat ?? Chat::build();

        return (new static())
            ->setAuthor($author)
            ->setText($text)
            ->setChat($chat);
    }

    public function __construct ()
    {
        $this->created = new DateTime();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getChat(): ?Chat
    {
        return $this->chat;
    }

    public function setChat(?Chat $chat): static
    {
        $this->chat = $chat;
        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): static
    {
        $this->author = $author;
        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): static
    {
        $this->text = $text;
        return $this;
    }

    public function getCrated(): DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): static
    {
        $this->created = $created;
        return $this;
    }

    public function isReaded(): bool
    {
        return $this->readed;
    }

    public function setReaded(bool $readed = null): static
    {
        $this->readed = $readed;
        return $this;
    }
}