<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle\Security;

use Allmega\MessageBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\MessageBundle\Entity\Chat;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ChatVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected string $icon = 'icon';

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'message-chat', [$this->icon]);
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        $isUser = $this->hasRole($user, Data::USER_ROLE);
        if (!$isUser || !$this->isSettedAndSupports($attribute, $subject)) return false;

        $isManager = $this->hasRole($user, Data::MANAGER_ROLE);
        $isAuthor = $subject && $this->isSameUser($user, $subject->getAuthor());

        switch ($attribute) {
            case $this->dashboard:
            case $this->icon:
            case $this->list:
            case $this->add:
                $result = true;
                break;
            case $this->edit:
                $result = $isAuthor;
                break;
            case $this->delete:
                $result = $isAuthor || $isManager;
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof Chat;
    }
}