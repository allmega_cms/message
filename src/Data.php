<?php

/**
 * This file is part of the Allmega Message Bundle package.
 *
 * @copyright Allmega 
 * @package   Message Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MessageBundle;

use Allmega\BlogBundle\Utils\Register\Entries\{ControllerEntriesMap, ControllerEntry};
use Allmega\MessageBundle\Controller\ChatController;
use Allmega\BlogBundle\Model\PackageData;

class Data extends PackageData
{
	public const DOMAIN = 'AllmegaMessageBundle';
	public const PACKAGE = 'message';

    public const NOTIFICATION_MESSAGE_RECEIVED = 'message_received';

    public const GROUP_TYPE_MESSAGE = 'message.main';

    public const MANAGER_GROUP = 'message.manager';
    public const USER_GROUP = 'message.user';

    public const MANAGER_ROLE = 'ROLE_MESSAGE_MANAGER';
	public const USER_ROLE = 'ROLE_MESSAGE_USER';

	protected function setRegisterData(): void
	{
        $this->package = self::PACKAGE;
        $this->data = [
            'controllerEntries' => $this->getControllerEntries(),
            'webpackEntries' => $this->getWebpackEntries(),
        ];
	}

    protected function setLoadData(): void
    {
        $this->package = self::PACKAGE;
        $this->data = [
            'notificationtypes' => $this->getNotificationTypes(),
            'grouptypes' => $this->getGroupTypes(),
            'groups' => $this->getGroups(),
            'roles' => $this->getRoles(),
            'users' => $this->getUsers(),
        ];
    }

	/**
	 * @return array<int,ControllerEntry>
	 */
	protected function getControllerEntries(): array
	{
		return [
			new ControllerEntry(
                ControllerEntriesMap::DASHBOARD_BOXES,
                self::PACKAGE,
                'chat',
                ChatController::class,
                'getDashboardWidget'),
			new ControllerEntry(
                ControllerEntriesMap::PROFILE_MENU,
                self::PACKAGE,
                'chat',
                ChatController::class,
                'getMenuIcon'),
		];
	}

    protected function getAuthData(): array
    {
        return [
            self::GROUP_TYPE_MESSAGE => [
                self::MANAGER_GROUP => [
                    self::MANAGER_ROLE => 'message.manager',
                    self::USER_ROLE => 'message.user',
                ],
                self::USER_GROUP => [
                    self::USER_ROLE => 'message.user',
                ],
            ],
        ];
    }

	protected function getNotificationTypesData(): array
	{
        return [self::NOTIFICATION_MESSAGE_RECEIVED];
	}
}