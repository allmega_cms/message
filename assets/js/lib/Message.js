import {Events} from './Events'

export default class Message
{
    #answerFormId = '#allmega-message-answer-form'
    #parentAnswerId = '.allmega-comment-list-item'
    #cancelAnswerId = '#allmega-cancel-answer'
    #sliderContent = '.allmega-slider-content'
    #messageDataId = '#allmega-message-data'
    #sliderArrow = '.allmega-slider-arrow'
    #sliderBody = '.allmega-slider-body'
    #loaderImg = '.allmega-loader-img'
    #rootList = '.allmega-root-list'
    #sendBtn = 'button.allmega-btn'
    #replyBtn = '.allmega-reply'
    #fas = 'fa-solid fa-angle-'
    #answerForm
    #sendData
    #url

    constructor()
    {
        let elem = $(this.#messageDataId)
        if (elem.length) this.#init(elem).#registerEvents()
    }

    #init(elem)
    {
        $(this.#rootList).find(this.#sliderBody).slideDown()
        $(this.#answerFormId).hide()
        this.#url = elem.data('url')
        return this
    }

    #registerEvents()
    {
        let self = this
        $(document).on('click', this.#replyBtn, function (event) {self.doReply(event, $(this))})
        $(document).on('click', this.#cancelAnswerId, () => this.#removeAnswerForm())
        $(document).on('click', this.#sendBtn, () => this.#send())
    }

    doReply(event, elem)
    {
        event.preventDefault()
        this.#createAnswerForm()
        
        let parentAnswer = elem.closest(this.#parentAnswerId).after(this.#answerForm)
        this.#sendData.setId(parentAnswer.attr('id'))

        let icon = this.#answerForm.find(this.#sliderArrow).first().children()
        let arrow = icon.attr('class') == this.#fas + 'up' ? 'down' : 'up'
        icon.attr('class', this.#fas + arrow)

        this.#answerForm.find('.alert').hide()
        this.#answerForm.show().find(this.#sliderBody).slideDown()
    }

    #createAnswerForm()
    {
        this.#removeAnswerForm()
        this.#answerForm = $(this.#answerFormId).clone()
        this.#sendData = new Data()
    }

    #removeAnswerForm()
    {
        if (this.#answerForm) this.#answerForm.remove()
        this.#sendData = null
    }

    #send()
    {
        let answer = this.#answerForm.find('textarea').val().trim()
        if (!answer) return
        
        $(document).trigger(Events.getBlogEvents().APP_LOADER_SHOW)
        this.#sendData.setAnswer(answer)

        this.#answerForm.find('button').hide().next().show()
        this.#answerForm.find('.alert').text('').hide()

        $.post(this.#url, this.#sendData.getData(), (data) => {
            console.log(data)
            if (data.success) this.#formToAnswer()
            else {     
                this.#answerForm.find('.alert').text(data.error).show()
                this.#answerForm.find('button').show().next().hide()
            }
            $(document).trigger(Events.getBlogEvents().APP_LOADER_HIDE)
        })
    }

    #formToAnswer()
    {
        this.#answerForm.find(this.#sliderContent).text(this.#sendData.getAnswer())
        this.#answerForm.find(this.#cancelAnswerId).remove()
        this.#answerForm.find(this.#loaderImg).remove()
        this.#answerForm.find('button').remove()
        this.#answerForm.removeAttr('id')
        this.#answerForm = null
    }
}

class Data
{
    #answer
    #id

    getAnswer()
    {
        return this.#answer
    }

    setAnswer(answer)
    {
        this.#answer = answer
        return this
    }

    getId()
    {
        return this.#id
    }

    setId(id)
    {
        this.#id = id
        return this
    }

    getData()
    {
        return {id: this.#id, answer: this.#answer}
    }
}