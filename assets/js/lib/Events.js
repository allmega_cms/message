import {Events as BlogEvents} from '../../../../blog/assets/js/lib/Events'

export class Events
{
    static #blogEvents

    static getBlogEvents()
    {
        if (!this.#blogEvents) this.#setBlogEvents()
        return this.#blogEvents
    }

    static #setBlogEvents()
    {
        this.#blogEvents = BlogEvents
    }
}