Installation
============

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

Applications that use Symfony Flex
----------------------------------

Open a command console, enter your project directory and execute:

```shell
$ composer require allmega/message
```

Applications that don't use Symfony Flex
----------------------------------------

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```shell
$ composer require allmega/message
```

### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    Allmega\MessageBundle\AllmegaMessageBundle::class => ['all' => true],
];
```

### Step 3: Register the Bundle

If bundle `allmega/blog` was not previously installed, 
refer to the installation instructions for this bundle

Subsequently open a command console, enter your project directory and execute:

```shell
$ yarn encore prod
```

Bundle is ready to use!
-----------------------
